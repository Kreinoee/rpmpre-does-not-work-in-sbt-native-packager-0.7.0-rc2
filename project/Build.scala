import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.SbtNativePackager.NativePackagerKeys
import sbt._
import Keys._
import scala.Some

object TestBuild extends Build {

  lazy val pimService = Project(
    id = "rpmpreBug",
    base = file("."),
    settings = Defaults.defaultSettings ++ packageArchetype.java_server ++ Seq (
	    organization := "bugTest",
	    version      := "1",
	    scalaVersion := "2.10.4",
	    crossScalaVersions := Seq("2.10.4"),

	    NativePackagerKeys.rpmVendor in Rpm := "Me",
        NativePackagerKeys.rpmPre := Some("echo This will properly be missing from the rpm"),
	    NativePackagerKeys.packageSummary := "This will properly be missing scriptlets that is defined as rpmPre",
	    NativePackagerKeys.packageDescription := "Bla bla",
	    NativePackagerKeys.rpmLicense := Some("Apache License 2.0"),
	    NativePackagerKeys.rpmBrpJavaRepackJars := true,
	    NativePackagerKeys.daemonUser in Linux := "native-packager-test-user",
	    NativePackagerKeys.daemonGroup in Linux := "native-packager-test-group",
	    NativePackagerKeys.rpmDistribution := Some("CentOS")
    )
  )
}


 

